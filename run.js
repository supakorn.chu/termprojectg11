var http = require('http');
var fs = require('fs');
var os = require("os");
http.createServer(function (req, res) {
    console.log(req.url);
    //console.log(req);
    if(req.method === 'GET'){
        var url = '';
        var index = req.url.indexOf('?');
        var path;
    if(index != -1)
        path = req.url.substring(0,index);
    else
        path = req.url;
    console.log('path: ' + path);
    switch(path){
        case '/studentInfo':
            url = 'json/studentData.json';
            break;
        case '/studentLogin':
            url = 'json/studentLogin.json';
            break;
        case '/teacherLogin':
            url = 'json/teacherLogin.json';
            break;
        case '/request':
            url = 'json/request.json';
            break;
        case '/login':
            url = 'Login_U1.html';
            break;
        case '/form':
            url = 'index_U2.html';
            break;
        case '/requirement':
            url = 'index_U3.html';
            break;
        case '/progress':
            url = 'index2_U5.html';
            break;
        case '/progress2':
            url = 'index_U5.html';
            break;
        case '/requestment':
            url = 'index2_U8.html';
            break;
        case '/requestment2':
            url = 'index_U8.html';
            break;
        case '/exit':
            process.exit();
            break;
        default:
            if(req.url.includes('.')){
                url = req.url;
                break;
            }
            url = 'redirect.html';
            break;
        }

        console.log(url);
        if(url){
            if(url.charAt(0) == '/'){
            url = url.substring(1);
            }
            fs.readFile(url, function(err, data) {
            
                console.log(err);
                if(err){
                    res.writeHead(404, {'Content-Type': 'text/html'});
                    res.write('<h1>404 NOT FOUND</h1>');
                    return res.end();
                } 
                else{
                    if(url.endsWith('.css')){
                        res.writeHead(200, {'Content-Type': 'text/css'});
                    }
                    else if(url.endsWith('.html')){
                        res.writeHead(200, {'Content-Type': 'text/html'});    
                    }
                    else if(url.endsWith('.js')){
                        res.writeHead(200, {'Content-Type': 'text/javascript'});
                    }
                    else if(url.endsWith('.json')){
                        res.writeHead(200, {'Content-Type': 'application/json'});
                    }
                    else{
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                    }
                    res.write(data);
                    return res.end();
                }
            });
        }
        else{
            res.end();
        }
    }    
    else if(req.method == "PUT"){
        var index = req.url.indexOf('?');
        var path;
        if(index != -1)
            path = req.url.substring(0,index);
        else
            path = req.url;
        console.log('path: ' + path);
        switch(path){
            case '/saveRequest':
                req.on('data',chunk=>{
                    //alert(chunk);
                    /*console.log(chunk);
                    var json = JSON.parse(chunk, null, 4);
                    json = JSON.stringify(json);*/
                    fs.writeFileSync('json/request.json', chunk);
                    res.writeHead(200,{'Content-Type': 'text/plain'});
                    res.write('OK');
                    res.end();
                });
                break;
            case '/saveStudentData':
                req.on('data',chunk=>{
                    //let json = JSON.parse(chunk, null, 4);
                    //json = JSON.stringify(json, null, 4);
                    fs.writeFileSync('json/studentData.json', chunk);
                    res.writeHead(200,{'Content-Type': 'text/plain'});
                    res.write('OK');
                    res.end();
                });
                break;
        }
    }
    else{
        res.end();
    }

}).listen(8080);

